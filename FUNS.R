waters_extract_time <- function(filepath){
    require(dplyr)

    if(!file.exists(paste0(filepath,"/_HEADER.TXT"))) return(data_frame(acquired_date = NA, acquired_time = NA))

    paste0(filepath,"/_HEADER.TXT") %>%
        readLines(n=25, ok=TRUE, warn=FALSE) %>%
        grep("Acquired Date|Acquired Time",.,value = TRUE) %>%
        regmatches(.,gregexpr("(?<=:).*",.,perl=TRUE)) %>%
        unlist %>%
        {data_frame(acquired_date = .[1], acquired_time = .[2])}

}



waters_extract_inlet <- function(filepath){
    require(dplyr)

    template <- data_frame(run_time=NA,
                           solvent_A = NA,
                           solvent_B = NA,
                           min_pressure = NA,
                           max_pressure = NA,
                           avg_pressure = NA,
                           column_type = NA,
                           column_serial = NA,
                           column_temp = NA,
                           gradient = NA
                          )

    if(!file.exists(paste0(filepath,"/_INLET.INF"))) return(template)

    txt <- paste0(filepath,"/_INLET.INF") %>%
                readLines(warn=FALSE) %>%
                gsub(",", ".", .)# same machine seems to be able to record differently!



    if(!(any(grepl("-- PUMP --",txt)) & any(grepl("-- AUTOSAMPLER --",txt)))) return(template)


    # run time
    run_time <- txt %>%
                    grep("Run Time",.,value = TRUE) %>%
                    .[1] %>%
                    regmatches(.,gregexpr("(?<=:).*[^min]+",.,perl=TRUE)) %>%
                    trimws() %>%
                    as.numeric()
    # Solvents
    solvent_A <- txt %>%
                    grep("Solvent Name A",.,value = TRUE) %>%
                    regmatches(.,gregexpr("(?<=:).*",.,perl=TRUE)) %>%
                    trimws()

    solvent_B <- txt %>%
                    grep("Solvent Name B",.,value = TRUE) %>%
                    regmatches(.,gregexpr("(?<=:).*",.,perl=TRUE)) %>%
                    trimws()

    # Column pressures
    min_pressure <- txt %>%
                    grep("Minimum System Pressure",.,value = TRUE) %>%
                    regmatches(.,gregexpr("(?<=:).*",.,perl=TRUE)) %>%
                    trimws() %>%
                    as.numeric() %>%
                    .[1]

    max_pressure <- txt %>%
                    grep("Maximum System Pressure",.,value = TRUE) %>%
                    regmatches(.,gregexpr("(?<=:).*",.,perl=TRUE)) %>%
                    trimws() %>%
                    as.numeric() %>%
                    .[1]

    avg_pressure <- txt %>%
                    grep("Average System Pressure",.,value = TRUE) %>%
                    regmatches(.,gregexpr("(?<=:).*",.,perl=TRUE)) %>%
                    trimws() %>%
                    as.numeric() %>%
                    .[1]

    # Column used
    column_type <- txt %>%
                    grep("Column Type",.,value = TRUE) %>%
                    regmatches(.,gregexpr("(?<=:).*",.,perl=TRUE)) %>%
                    trimws() %>%
                    paste0(collapse=" //AND// ")

    column_serial <- txt %>%
                        grep("Column Serial Number",.,value = TRUE) %>%
                        regmatches(.,gregexpr("(?<=:).*",.,perl=TRUE)) %>%
                        trimws() %>%
                        paste0(collapse=" //AND// ")

    column_temp <- txt %>%
                        {grep("Target Column Temperature",.,value = TRUE)[1]} %>%
                        regmatches(.,gregexpr("(?<=:).*",.,perl=TRUE)) %>%
                        trimws()

    # Gradient
    start_pos <- txt %>% {grep("\\[Gradient Table\\]",.)+1} %>% min # in case of multiple LC/GC we take the first
    end_pos   <- txt %>% {grep(" [1-9]\\.",.)} %>% {.[.>start_pos]} %>% {min(.[diff(.)>1])}

    gradient <- txt[start_pos:end_pos] %>%
                    trimws() %>%
                    gsub("Initial", "NA", .) %>%
                    gsub("Flow Rate", "Flow_Rate", .) %>%
                    {read.csv(text=., sep=" ", stringsAsFactors=FALSE)} %>%
                    as_data_frame %>%
                    rename(time = Time.min., flow_rate = Flow_Rate, A = X.A, B = X.B, curve = Curve) %>%
                    serialize(NULL, ascii=TRUE) %>%
                    rawToChar() #%>%
                    # charToRaw() %>%
                    # unserialize()



    zero_length_NA <- function(x) ifelse(length(x)==0,NA,x)

    out <- data_frame(run_time = zero_length_NA(run_time),
                       solvent_A = zero_length_NA(solvent_A),
                       solvent_B = zero_length_NA(solvent_B),
                       min_pressure = zero_length_NA(min_pressure),
                       max_pressure = zero_length_NA(max_pressure),
                       avg_pressure = zero_length_NA(avg_pressure),
                       column_type = zero_length_NA(column_type),
                       column_serial = zero_length_NA(column_serial),
                       column_temp = zero_length_NA(column_temp),
                       gradient = zero_length_NA(gradient)
                      )

    return(out)
}




waters_extract_spl <- function(file){
    require(RODBC)

    con <- odbcConnectAccess2007(file)

    if(con==-1) return(data_frame())

    out <- sqlFetch(con, "ANALYSIS", stringsAsFactors = FALSE) %>%
			mutate_at(vars(one_of('SAMPLE_LOCATION', 'FILE_NAME')), funs(as.character)) %>%
            mutate_at(vars(contains('CONC_')), funs(as.character))
    close(con)

    return(out)
}



waters_path_to_project <- function(full_path){
    require(dplyr)

    strsplit(dirname(full_path),"/") %>%
        {sapply(., function(x) {nth(x, length(x)-1)})}
}


read_wlogbook_samples_db <- function(db_con){

    require(dbplyr)
    require(dplyr)
    require(chron)

db_con %>%
    tbl("samples") %>%
    collect() %>%
    mutate(acquired_date = as.Date(acquired_date, origin = "1970-01-01")) %>%
    mutate_at(vars(mtime, ctime, atime), funs(as.POSIXct(., origin = "1970-01-01"))) %>%
    mutate(acquired_time = times(acquired_time)) %>%
    arrange((FILE_NAME))

}


read_wlogbook_summary_db <- function(db_con){

    require(dbplyr)
    require(dplyr)
    require(chron)

db_con %>%
    tbl("summary") %>%
    collect() %>%
    mutate(acquired_date = as.Date(acquired_date, origin = "1970-01-01"))

}

